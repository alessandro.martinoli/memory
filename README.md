Use Parcel to build. Documentation at: https://parceljs.org/

run on main directory for develop:
```
parcel index.html
```

run on main directory for build:
```
parcel build index.html
```
