import filenames from './cards'

/** Game status, see resetState for default */
let state = {};
/** Game cards */
let cards = [];

/**
 * Get random property of an object
 * @param {Object} obj 
 */
var getRandomProperty = function (obj) {
    var keys = Object.keys(obj);
    return obj[keys[ keys.length * Math.random() << 0]];
};

/** Reset game page */
const reset = (container) => {
	container.innerHTML = "";
	resetState();
}

/** Set the initial state */
const resetState = () => {
    state = {
        moves: 0,
        points: 0,
		matrix: [],
		selected: null,
    }
}

/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 */
const shuffle = (a) => {
    for (let i = 0; i < a.length - 1; i++) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

/** Generate Guid */
const uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

/**
 * Create card DOM Element
 * @param {Object} options 
 */
const createCard = (options) => {
	const card = document.createElement('div');

	card.className = 'card';  
	card.id = options.id;

	/** add listener onclick */
	card.addEventListener('click', (e) => play(e.target));

	return card;
}

/**
 * Initialize game UI
 * @param {Number} n 
 */
const render = _ => {
	const container = document.getElementById("root");
	reset(container);

	shuffle(filenames).forEach((name) => {
		cards.push({
			id: uuidv4(),
			name: name,
			selected: false,
			guessed: false,
		}, {
			id: uuidv4(),
			name: name,
			selected: false,
			guessed: false,
		});
	});
	
	shuffle(cards).forEach(card => {
        container.appendChild(createCard(card));
	});
}

/**
 * Game control
 * @param {Object} card 
 */
const play = (card) => {
	let selected = cards.filter((card) => card.selected);

	if (
		cards.filter(c => c.id == card.id && c.guessed).length != 0
		|| selected.length == 2
		|| (selected.length == 1 && selected[0].id == card.id)
	) {
		return;
	} else {
		selected.push(flip(card));
	}
	
	if(selected.length == 2){
		setTimeout(() => {
			state.moves++;
			
			if(selected[0].name == selected[1].name){
				state.points++;
				
				selected.forEach(card => {
					document.getElementById(card.id).style.background = 'none';
					card.guessed = true;
					card.selected = false;
				});
			} else {
				selected.forEach(card => flip(document.getElementById(card.id)));
			}

			win();
		}, 500);
	}
}

/**
 * Check win condition and execute win action
 */
const win = () => {
	if(state.points == cards.length / 2){
		alert('Hai vinto in ' + state.moves + ' mosse');
	}
}

/**
 * Flip a card
 * @param {Object} selected 
 */
const flip = (selected) => {
	let card = cards.filter((card) => card.id == selected.id)[0];
	
	selected.style.backgroundImage = !card.selected ? "URL(images/" + card.name + ".png)" : "";
	
	card.selected = !card.selected;

	return card;
}

window.onload = _ => render();