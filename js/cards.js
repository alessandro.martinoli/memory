/** Card filenames */
const filenames = [
	'acero_campestre',
	'biancospino',
	'carpino_bianco',
	'cerro',
	'ciliegio_selvatico',
	'corniolo',
	'edera',
	'farnia',
	'ligustro',
	'melo_selvatico',
	'nocciolo',
	'olmo',
	'ontano',
	'pioppo_nero',
	'robinia',
	'rosa',
	'rovere',
	'salice_bianco',
	'tiglio',
]

export default filenames;